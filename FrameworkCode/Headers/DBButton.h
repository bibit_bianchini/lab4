/****************************************************************************
 
  Header file for Debounce Button Module

 ****************************************************************************/

#ifndef DBButton_H
#define DBButton_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    /* for ES_Event_t reference */

// typedefs for the states
// State definitions for use with the query function
typedef enum { Debouncing, Ready2Sample } ButtonDebounceState_t ;

// Public Function Prototypes
bool PostButtonDebounce(ES_Event_t ThisEvent);
bool InitButtonDebounce(uint8_t Priority);
ES_Event_t RunButtonDebounce(ES_Event_t ThisEvent);
bool CheckButtonEvents(void);

#endif // DBButton_H
