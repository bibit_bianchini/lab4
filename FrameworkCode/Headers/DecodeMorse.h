/****************************************************************************
 
  Header file for Decode Morse Service

 ****************************************************************************/

#ifndef DecodeMorse_H
#define DecodeMorse_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    /* for ES_Event_t reference */

// Public Function Prototypes
bool PostDecodeMorse(ES_Event_t ThisEvent);
bool InitDecodeMorse(uint8_t Priority);
ES_Event_t RunDecodeMorse(ES_Event_t ThisEvent);

#endif // DecodeMorse_H
