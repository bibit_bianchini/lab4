/****************************************************************************
 
  Header file for LCD Service

 ****************************************************************************/

#ifndef LCDService_H
#define LCDService_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    /* for ES_Event_t reference */

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitPState, Initializing, Waiting2Write, 
               PausingBetweenWrites } LCDState_t ;

// Public Function Prototypes
bool InitLCDService ( uint8_t Priority );
bool PostLCDService( ES_Event_t ThisEvent );
ES_Event_t RunLCDService( ES_Event_t ThisEvent );


#endif // LCDService_H
