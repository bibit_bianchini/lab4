/****************************************************************************
 
  Header file for Morse Elements Service

 ****************************************************************************/

#ifndef MorseElements_H
#define MorseElements_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    /* for ES_Event_t reference */

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitMorseElements, CalWaitForRise, CalWaitForFall,
               EOC_WaitRise, EOC_WaitFall, DecodeWaitRise, DecodeWaitFall }
						 MorseElementsState_t ;

// Public Function Prototypes
bool InitializeMorseElements(uint8_t Priority);
bool PostMorseElements(ES_Event_t ThisEvent);
ES_Event_t RunMorseElements(ES_Event_t ThisEvent);
bool CheckMorseEvents(void);

#endif // MorseElements_H
