/***************************************
Test harness for low level LCD functions
Lab 3, Part 3.1
***************************************/


#define TEST


#ifdef TEST
#include "termio.h"
#include "ES_Port.h"
#include "LCD_Write.h"

int main(void)
{
  TERMIO_Init();
  puts("\r\n\n\nIn test harness for LCD_Write\r\n");
  
 	printf("%s %s\n",__TIME__, __DATE__);
 	printf("\n\r\n");
  
  LCD_HWInit();
  
  puts("\r\nPress any key to try `LCD_WriteCommand4(00000000)`\r\n");
  getchar();
  LCD_WriteCommand4(0x00);
  printf("\r\nJust sent %d.\r\n", 0x00);
  
  puts("\r\nPress any key to try `LCD_WriteCommand4(11110000)`\r\n");
  getchar();
  LCD_WriteCommand4(0xf0);
  puts("\r\nJust sent 11110000.\r\n");
  
  puts("\r\nPress any key to try `LCD_WriteCommand4(11111111)`\r\n");
  getchar();
  LCD_WriteCommand4(0xff);
  puts("\r\nJust sent 11111111.\r\n");
  
  puts("\r\nPress any key to try `LCD_WriteCommand4(11111111)`\r\n");
  getchar();
  LCD_WriteCommand4(0xff);
  puts("\r\nJust sent 11111111.\r\n");
  
  puts("\r\nPress any key to try `LCD_WriteCommand4(00000011)`\r\n");
  getchar();
  LCD_WriteCommand4(0x03);
  puts("\r\nJust sent 00000011.\r\n");
  
  puts("\r\nPress any key to try `LCD_WriteData8(00000011)`\r\n");
  getchar();
  LCD_WriteData8(0x03);
  puts("\r\nJust sent 00000011 -- expected output:  0000 0010 then 0011 0010 \r\n");
  
}

#endif    // TEST
