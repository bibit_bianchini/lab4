/****************************************************************************
 Module
   DecodeMorseService.c

 Revision
   1.0.1

 Description
   This is a DecodeMorse file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from DecodeMorseFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "DecodeMorseService.h"

#include <string.h>

/*----------------------------- Module Defines ----------------------------*/
static char LegalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= !$&+;@_";
static char MorseCode[][8] ={ ".-","-...","-.-.","-..",".","..-.","--.","....","..",
											".---","-.-",".-..","--","-.","---",".--.","--.-",".-.",
											"...","-","..-","...-",".--","-..-","-.--","--..",".----",
											"..---","...--","....-",".....","-....","--...","---..",
											"----.","-----","..--..",".-.-.-","--..--","---...",
											".----.","-....-","-..-.","-.--.-","-.--.-",".-..-.",
											"-...-","-.-.--","...-..-",".-...",".-.-.","-.-.-.",
											".--.-.","..--.-"};
static char MorseString[8];

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDecodeMorseService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitDecodeMorseService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostDecodeMorseService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDecodeMorseService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDecodeMorseService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDecodeMorseService(ES_Event_t ThisEvent)
{
  printf("\r\nEnter Run\r\n");
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
	switch (ThisEvent.EventType) {
		case DotDetected : 
    {
      printf("  --> inside RunDecodeMorse, detected a dot");
			//If there is room for another Morse element in the internal representation
			if (strlen(MorseString) < 8) {
				//Add a Dot to the internal representation
				strcat(MorseString, ".");
        printf("\r\n  Added a dot, Morse string is %s", MorseString);
			}
			else {
				//Set ReturnValue to ES_ERROR with param set to indicate this location
				ReturnEvent.EventType = ES_ERROR;
				ReturnEvent.EventParam = strlen(MorseString);
        printf("\r\n  Error");
			}
    }
    break;
  }
  return ReturnEvent;
} //End RunDecodeMorseService
  
/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

