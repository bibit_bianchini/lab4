/****************************************************************************
 Module
   DecodeMorse.c

 Description
   Decode Morse is a service developed for Lab 4 in order to handle all
	 character representation involved with the Morse code elements.  It takes
	 in the dot/dash/space events and returns characters.

 Notes
 Data private to the module: MorseString, the arrays LegalChars and MorseCode

 History
 When           Who     What/Why
 -------------- ---     --------
 11/01/18       bibit   lab 4

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "DecodeMorse.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Timers.h"
#include "ES_Port.h"

/* include header files for the other modules in Lab4 that are referenced
*/
#include <string.h>
#include "LCDService.h"

/*----------------------------- Module Defines ----------------------------*/
static char LegalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= !$&+;@_";
static char MorseCode[][8] ={ ".-","-...","-.-.","-..",".","..-.","--.","....",
                      "..",".---","-.-",".-..","--","-.","---",".--.","--.-",
                      ".-.","...","-","..-","...-",".--","-..-","-.--","--..",
                      ".----","..---","...--","....-",".....","-....","--...",
                      "---..","----.","-----","..--..",".-.-.-","--..--",
                      "---...",".----.","-....-","-..-.","-.--.-","-.--.-",
                      ".-..-.","-...-","-.-.--","...-..-",".-...",".-.-.",
                      "-.-.-.",".--.-.","..--.-"};
static char MorseString[8];

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service. They should be
	 functions relevant to the behavior of this service.
*/
static char DecodeMorseString(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDecodeMorse

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, true

 Description
     Saves away the priority, and does any other required initialization for
		 this service.
		 
 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
bool InitDecodeMorse(uint8_t Priority) {
  ES_Event_t ThisEvent;
	// Initialize the MyPriority variable with the passed in parameter.
	MyPriority = Priority;
	// Clear (empty) the MorseString variable
	strcpy(MorseString, "");
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true) {
    return true;
  }
  else {
    return false;
  }
}

/****************************************************************************
 Function
     PostDecodeMorse

 Parameters
     ES_Event_t ThisEvent, the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
bool PostDecodeMorse(ES_Event_t ThisEvent) {
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunDecodeMorse

 Parameters
		 ThisEvent: will be one of:  DotDetectedEvent, DashDetectedEvent,
																 EOCDetected, EOWDetected, ButtonDown

 Returns
     ES_NO_EVENT if no error detected, ES_ERROR otherwise

 Description
     Implements the state machine for Decode Morse

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
ES_Event_t RunDecodeMorse(ES_Event_t ThisEvent) {
	// Local var ReturnValue initialized to ES_NO_EVENT
	ES_Event_t ReturnValue;
	ReturnValue.EventType = ES_NO_EVENT;
	// Based on the state of the ThisEvent variable choose one of the following
	// blocks of code:
	switch (ThisEvent.EventType) {
		case DotDetected : {
			// If there is room for another Morse element in the internal
      // representation:
			if (strlen(MorseString) < 8) {
				// Add a Dot to the internal representation
				strcat(MorseString, ".");
			}
			else {
				// Set ReturnValue to ES_ERROR with param set to indicate this location
				ReturnValue.EventType = ES_ERROR;
				ReturnValue.EventParam = strlen(MorseString);
			}
    }
		break;

		case DashDetected : {
			// If there is room for another Morse element in the internal
      // representation
			if (strlen(MorseString) < 8) {
				// Add a Dash to the internal representation
				strcat(MorseString, "-");
			}
			else {
				// Set ReturnValue to ES_ERROR with param set to indicate this location
				ReturnValue.EventType = ES_ERROR;
				ReturnValue.EventParam = strlen(MorseString);
			}
    }
		break;
		
		case EOCDetected : {
			// Call DecodeMorse to try and match current MorseString
			char NextChar = DecodeMorseString();
			// Print to LCD the decoded character
			ES_Event_t LCDEvent;
			LCDEvent.EventType = ES_LCD_PUTCHAR;
			LCDEvent.EventParam = NextChar;
			PostLCDService(LCDEvent);
			// Clear (empty) the MorseString variable
			strcpy(MorseString, ""); 
    }
		break;

		case EOWDetected : {
			// Call DecodeMorse to try and match current MorseString
			char NextChar = DecodeMorseString();
			// Print to LCD the decoded character
			ES_Event_t LCDEvent;
			LCDEvent.EventType = ES_LCD_PUTCHAR;
			LCDEvent.EventParam = NextChar;
			PostLCDService(LCDEvent);
			// Print to the LCD a space
			ES_Event_t LCDEvent_2;
			LCDEvent_2.EventType = ES_LCD_PUTCHAR;
			LCDEvent_2.EventParam = ' ';
			PostLCDService(LCDEvent_2);
			// Clear (empty) the MorseString variable
			strcpy(MorseString, ""); 
    }
		break;

		case BadSpace :
			// Clear (empty) the MorseString variable
			strcpy(MorseString, "");
		break;
		
		case BadPulse :
			// Clear (empty) the MorseString variable
			strcpy(MorseString, "");
		break;

		case DBButtonDown :
			// Clear (empty) the MorseString variable
			strcpy(MorseString, "");
		break;
	}
	// Return ReturnValue
	return ReturnValue;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
     DecodeMorseString

 Parameters
		 None

 Returns
     A character or a '~' indicating failure

 Description

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
static char DecodeMorseString(void) {
	// For every entry in the array MorseCode
	for (int count=0; count<(sizeof(LegalChars)/sizeof(LegalChars[0])); count++){
		// If MorseString is the same as current position in MorseCode 
		if (strcmp(MorseString, MorseCode[count])==0) {
			// Return contents of current position in LegalChars
			return LegalChars[count];
		}
	}
	// Return '~', since we didn't find a matching string in MorseCode
	return '~';
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
