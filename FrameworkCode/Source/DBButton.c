/****************************************************************************
 Module
   DBButton.c

 Description
   Debounce Button is a service developed for Lab 4 in order to debounce the
   output from the recalibrate button.  It takes in the data from the button
   pin and returns button pressed/unpressed events.

 Notes
   Data private to the module: LastButtonState

 History
 When           Who     What/Why
 -------------- ---     --------
 11/02/18       bibit   lab 4

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "DBButton.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Timers.h"
#include "ES_Port.h"

/* include header files for the other modules in Lab4 that are referenced
*/

/*----------------------------- Module Defines ----------------------------*/
static uint8_t LastButtonState;
static ButtonDebounceState_t CurrentState;

// these times assume a 1.000mS/tick timing
#define DebounceTime 20

#define BUTTON_LINE_HI BIT4HI
#define BUTTON_LINE_LO BIT4LO

#define ButtonTimer 2

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service. They should be
	 functions relevant to the behavior of this service.
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitButtonDebounce

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, true

 Description
     Saves away the priority, and does any other required initialization for
		 this service.
		 
 Notes

 Author
     Bibit Bianchini, 11/02/2018
****************************************************************************/
bool InitButtonDebounce(uint8_t Priority) {
  // Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
  // Initialize the port line to monitor the button
	// --set up port B by enabling the peripheral clock
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;
  // --wait for the peripheral to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI) {}
  // --initialize PB4 to be a digital input
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= BUTTON_LINE_HI;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= BUTTON_LINE_LO;
  // Sample the button port pin and use it to initialize LastButtonState
	LastButtonState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS));
  // Set CurrentState to be Debouncing
  CurrentState = Debouncing;
  // Start debounce timer (timer posts to ButtonDebounceSM)
  ES_Timer_InitTimer(ButtonTimer, DebounceTime);
  return true;
}

/****************************************************************************
 Function
     PostButtonDebounce

 Parameters
     ES_Event_t ThisEvent, the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Notes

 Author
     Bibit Bianchini, 11/02/2018
****************************************************************************/
bool PostButtonDebounce(ES_Event_t ThisEvent) {
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunButtonDebounce

 Parameters
		 ThisEvent: will be one of:  ButtonUp, ButtonDown, or ES_TIMEOUT

 Returns
     ES_NO_EVENT if no error detected, ES_ERROR otherwise

 Description
     Implements the 2-state state machine for Button Debounce

 Notes

 Author
     Bibit Bianchini, 11/02/2018
****************************************************************************/
ES_Event_t RunButtonDebounce(ES_Event_t ThisEvent) {
  
  switch (CurrentState) {
    case (Debouncing) :
      // If EventType is ES_TIMEOUT & parameter is debounce timer number
      if ((ThisEvent.EventType == ES_TIMEOUT) &
          (ThisEvent.EventParam == ButtonTimer)) {
        // Set CurrentState to Ready2Sample
        CurrentState = Ready2Sample;
      }
	  break;
    case (Ready2Sample) :
      // If EventType is ButtonUp
      if (ThisEvent.EventType == ButtonUp) {
        // Start debounce timer
        ES_Timer_InitTimer(ButtonTimer, DebounceTime);
        // Set CurrentState to Debouncing
        CurrentState = Debouncing;
        // Post DBButtonUp to MorseElements & DecodeMorse queues
        ES_Event_t ThisEvent;
        ThisEvent.EventType = DBButtonUp;
        ES_PostList01(ThisEvent);
      }
      // If EventType is ButtonDown
      if (ThisEvent.EventType == ButtonDown) {
        // Start debounce timer
        ES_Timer_InitTimer(ButtonTimer, DebounceTime);
        // Set CurrentState to Debouncing
        CurrentState = Debouncing;
        // Post DBButtonDown to MorseElements & DecodeMorse queues
        ES_Event_t ThisEvent;
        ThisEvent.EventType = DBButtonDown;
        ES_PostList01(ThisEvent);
      }
	  break;
  }
  // Return ES_NO_EVENT
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}

/****************************************************************************
 Function
     CheckButtonEvents

 Parameters
     None

 Returns
     bool, true if an event was posted, false otherwise

 Description
		 The event-checker for the DBButton service.

 Notes

 Author
     Bibit Bianchini, 11/02/2018
****************************************************************************/
bool CheckButtonEvents(void) {
  // Local ReturnVal = False
  bool ReturnVal = false;
  // Set CurrentButtonState to state read from port pin
	uint8_t CurrentButtonState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS));
  // If the CurrentButtonState is different from the LastButtonState
	if ((CurrentButtonState&BUTTON_LINE_HI)!=(LastButtonState&BUTTON_LINE_HI)) {
    ES_Event_t ThisEvent;
    // Set ReturnVal = True
    ReturnVal = true;
    // If the CurrentButtonState is down:
    if ((CurrentButtonState & BUTTON_LINE_HI) == 0) {
      // PostEvent ButtonDown to ButtonDebounce queue
			ThisEvent.EventType = ButtonDown;
			PostButtonDebounce(ThisEvent);
    }
    // Else (CurrentButtonState is up):
    else {
      // PostEvent ButtonUp to ButtonDebounce queue
      ThisEvent.EventType = ButtonUp;
      PostButtonDebounce(ThisEvent);
    }
  }
  // Set LastButtonState to the CurrentButtonState
  LastButtonState = CurrentButtonState;
  // Return ReturnVal
  return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

