/****************************************************************************
 Module
   MorseElements.c

 Description
   Morse Elements is a service developed for Lab 4 in order to handle all
	 timing involved with the Morse code elements.  It takes in the data stream
	 and returns dot/dash/space events.

 Notes
 Data private to the module: MyPriority, CurrentState, TimeOfLastRise,
														 TimeOfLastFall, LengthOfDot, FirstDelta
														 LastInputState

 History
 When           Who     What/Why
 -------------- ---     --------
 11/01/18       bibit   lab 4

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "MorseElements.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Timers.h"
#include "ES_Port.h"

/* include header files for the other modules in Lab4 that are referenced
*/
#include "DecodeMorse.h"

/*----------------------------- Module Defines ----------------------------*/

#define MORSE_LINE_HI BIT3HI
#define MORSE_LINE_LO BIT3LO

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service. They should be
	 functions relevant to the behavior of this service.
*/
static void CharacterizePulse(void);
static void CharacterizeSpace(void);
static void TestCalibration(void);

/*---------------------------- Module Variables ---------------------------*/
// With the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t LastInputState;
static uint16_t TimeOfLastRise, TimeOfLastFall, LengthOfDot, FirstDelta;
static MorseElementsState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitializeMorseElements

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any other required initialization for
		 this service.
		 
 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
bool InitializeMorseElements(uint8_t Priority) {
	// Initialize the MyPriority variable with the passed in parameter.
	MyPriority = Priority;
	// Initialize the port line to receive Morse code
	// --set up port B by enabling the peripheral clock
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;
  // --wait for the peripheral to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI) {}
  // --initialize PB3 to be a digital input
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= MORSE_LINE_HI;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= MORSE_LINE_LO;
	
	// Sample port line and use it to initialize the LastInputState variable
	LastInputState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS));
	// Set CurrentState to be InitMorseElements
	CurrentState = InitMorseElements;
	// Set FirstDelta to 0
	FirstDelta = 0;
	// Post Event ES_Init to MorseElements queue (this service)
  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
	if (ES_PostToService( MyPriority, ThisEvent) == true) {
    return true;
  }
  else {
    return false;
  }
}

/****************************************************************************
 Function
     PostMorseElements

 Parameters
     EF_Event ThisEvent, the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Notes

 Author
     Bibit Bianchini, 11/02/2018
****************************************************************************/
bool PostMorseElements(ES_Event_t ThisEvent) {
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunMorseElements

 Parameters
		 ThisEvent: will be one of:  ES_Init, RisingEdge, FallingEdge,
																 CalibrationCompleted, EOCDetected,
																 DBButtonDown
		 The parameter field of the ThisEvent will be the time that the event
		 occurred.

 Returns
     ES_NO_EVENT (0)

 Description
     Implements the state machine for Morse Elements

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
ES_Event_t RunMorseElements(ES_Event_t ThisEvent) {
	// Set NextState to CurrentState
  MorseElementsState_t NextState = CurrentState;
	switch (CurrentState){
    case InitMorseElements :
			// If ThisEvent is ES_INIT:
      if (ThisEvent.EventType == ES_INIT) {
        // Set NextState to CalWaitForRise
				NextState = CalWaitForRise;
      }
      break;
			
		case CalWaitForRise :
			// If ThisEvent is RisingEdge:
			if (ThisEvent.EventType == RisingEdge) {
				// Set TimeOfLastRise to Time from event parameter
				TimeOfLastRise = ThisEvent.EventParam;
	      // Set NextState to CalWaitForFall
				NextState = CalWaitForFall;
		  }
			// If ThisEvent is CalibrationComplete:
			if (ThisEvent.EventType == CalibrationComplete) {
				// Set NextState to EOC_WaitRise
				printf("\r\nFinished Calibration (Dot time %d)\r\n", LengthOfDot);
				NextState = EOC_WaitRise;
			}
			break;
			
		case CalWaitForFall :
			// If ThisEvent is FallingEdge:
			if (ThisEvent.EventType == FallingEdge) {
				// Set TimeOfLastFall to Time from event parameter
				TimeOfLastFall = ThisEvent.EventParam;
				// Set NextState to CalWaitForRise
				NextState = CalWaitForRise;
				// Call TestCalibration function
				TestCalibration();
			}
			break;
			
		case EOC_WaitRise :
			// If ThisEvent is RisingEdge:
			if (ThisEvent.EventType == RisingEdge) {
				// Set TimeOfLastRise to Time from event parameter
				TimeOfLastRise = ThisEvent.EventParam;
				// Set NextState to EOC_WaitFall
				NextState = EOC_WaitFall;
				// Call CharacterizeSpace function
				CharacterizeSpace();
			}
			// If ThisEvent is DBButtonDown:
			if (ThisEvent.EventType == DBButtonDown) {
				// Set NextState to CalWaitForRise
				NextState = CalWaitForRise;
				// Set FirstDelta to 0
				FirstDelta = 0;
			}
			break;
		
		case EOC_WaitFall :
			// If ThisEvent is FallingEdge:
			if (ThisEvent.EventType == FallingEdge) {
				// Set TimeOfLastFall to Time from event parameter
				TimeOfLastFall = ThisEvent.EventParam;
				// Set NextState to EOC_WaitRise
				NextState = EOC_WaitRise;
			}
			// If ThisEvent is DBButtonDown:
			if (ThisEvent.EventType == DBButtonDown) {
				// Set NextState to CalWaitForRise
				NextState = CalWaitForRise;
				// Set FirstDelta to 0
				FirstDelta = 0;
			}
			// If ThisEvent is EOCDetected:
			if (ThisEvent.EventType == EOCDetected) {
				// Set the NextState to DecodeWaitFall
				NextState = DecodeWaitFall;
			}
			break;
		
		case DecodeWaitRise :
			// If ThisEvent is RisingEdge:
			if (ThisEvent.EventType == RisingEdge) {
				// Set TimeOfLastRise to Time from event parameter
				TimeOfLastRise = ThisEvent.EventParam;
				// Set NextState to DecodeWaitFall
				NextState = DecodeWaitFall;
				// Call CharacterizeSpace function
				CharacterizeSpace();
			}
			// If ThisEvent is DBButtonDown:
			if (ThisEvent.EventType == DBButtonDown) {
				// Set NextState to CalWaitForRise
				NextState = CalWaitForRise;
				// Set FirstDelta to 0
				FirstDelta = 0;
			}
			break;
		
		case DecodeWaitFall :
			// If ThisEvent is FallingEdge:
			if (ThisEvent.EventType == FallingEdge) {
				// Set TimeOfLastFall to Time from event parameter
				TimeOfLastFall = ThisEvent.EventParam;
				// Set NextState to DecodeWaitRise
				NextState = DecodeWaitRise;
				// Call CharacterizePulseFunction
				CharacterizePulse();
			}
			// If ThisEvent is DBButtonDown:
			if (ThisEvent.EventType == DBButtonDown) {
				// Set NextState to CalWaitForRise
				NextState = CalWaitForRise;
				// Set FirstDelta to 0
				FirstDelta = 0;
			}
			break;
	}
	// Set CurrentState to NextState
	CurrentState = NextState;
	// Return ES_NO_EVENT
	ES_Event_t ReturnValue;
	ReturnValue.EventType = ES_NO_EVENT;
	return ReturnValue;
}

/****************************************************************************
 Function
     CheckMorseEvents

 Parameters
     None

 Returns
     bool, true if an event was posted, false otherwise

 Description
		 The event-checker for the MorseElements service.

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
bool CheckMorseEvents(void) {
	bool ReturnVal = false;
	// Get the CurrentInputState from the input line
	uint8_t CurrentInputState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS));
	// If the state of the Morse input line has changed:
	if ((CurrentInputState&MORSE_LINE_HI) != (LastInputState&MORSE_LINE_HI)) {
    ES_Event_t ThisEvent;
		// If the current state of the input line is high:
		if ((CurrentInputState&MORSE_LINE_HI)!= false) {
			// PostEvent RisingEdge with parameter of the Current Time
			ThisEvent.EventType = RisingEdge;
			ThisEvent.EventParam = ES_Timer_GetTime();
			PostMorseElements(ThisEvent);
		}
		// Else (current input state is low):
		else {
			// PostEvent FallingEdge with parameter of the Current Time
			ThisEvent.EventType = FallingEdge;
			ThisEvent.EventParam = ES_Timer_GetTime();
			PostMorseElements(ThisEvent);
		}
		// Set ReturnVal = True
		ReturnVal = true;
	}
	// Set LastInputState to CurrentInputState
	LastInputState = CurrentInputState;
	// Return ReturnVal
	return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function
     TestCalibration

 Parameters
		 None

 Returns
     None

 Description

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
static void TestCalibration(void) {
	// Local variable SecondDelta
	uint16_t SecondDelta;
	// If calibration is just starting (FirstDelta is 0):
	if (FirstDelta == 0) {
		// Set FirstDelta to most recent pulse width
		FirstDelta = TimeOfLastFall - TimeOfLastRise;
	}
	else {
		// Set SecondDelta to most recent pulse width
		SecondDelta = TimeOfLastFall - TimeOfLastRise;
		// If (100.0 * FirstDelta / SecondDelta) less than or equal to 33.33
		if ((100.0 * FirstDelta / SecondDelta) <= 33.33) {
			// Save FirstDelta as LengthOfDot
			LengthOfDot = FirstDelta;
			// PostEvent CalibrationComplete to PostMorseElements
			ES_Event_t ThisEvent;
			ThisEvent.EventType = CalibrationComplete;
			PostMorseElements(ThisEvent);
		}
		// ElseIf (100.0 * FirstDelta / SecondDelta) greater than 300.0
		else if ((100.0 * FirstDelta / SecondDelta) > 300.0) {
			// Save SecondDelta as LengthOfDot
			LengthOfDot = SecondDelta;
			// PostEvent CalCompleted to MorseElementsSM
			ES_Event_t ThisEvent;
			ThisEvent.EventType = CalibrationComplete;
			PostMorseElements(ThisEvent);
		}
		// Else (prepare for next pulse)
		else {
			// SetFirstDelta to SecondDelta
			FirstDelta = SecondDelta;
		}
	}
}

/****************************************************************************
 Function
     CharacterizeSpace

 Parameters
		 None

 Returns
     None

 Description
		 Posts one of EOCDetected Event, EOWDetected Event, BadSpace Event as
		 appropriate.  On good dot-space, does nothing.

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
static void CharacterizeSpace(void) {
	// Calculate LastInterval as TimeOfLastRise � TimeOfLastFall
	uint16_t LastInterval = (TimeOfLastRise - TimeOfLastFall);
	// If LastInterval not OK for a Dot Space
	if ((LastInterval < (LengthOfDot-1)) | (LastInterval > (LengthOfDot+1))) {
		// If LastInterval OK for a Character Space
		if ((LastInterval>=((3*LengthOfDot)-3)) &
        (LastInterval<=((3*LengthOfDot)+3))) {
			// Post event EOCDetected to Decode Morse Service & Morse Elements
      // Service
			ES_Event_t ThisEvent;
			ThisEvent.EventType = EOCDetected;
			ES_PostList01(ThisEvent);
		}
		else {
			// If LastInterval OK for Word Space
			if ((LastInterval>=((7*LengthOfDot)-7)) &
          (LastInterval<=((7*LengthOfDot)+7))) {
				// PostEvent EOWDetected Event to Decode Morse Service
				ES_Event_t ThisEvent;
				ThisEvent.EventType = EOWDetected;
				PostDecodeMorse(ThisEvent);
			}
			else {
				// PostEvent BadSpace Event to Decode Morse Service
				ES_Event_t ThisEvent;
				ThisEvent.EventType = BadSpace;
				PostDecodeMorse(ThisEvent);
			}
		}
	}
}

/****************************************************************************
 Function
     CharacterizePulse

 Parameters
		 None

 Returns
     None

 Description
		 Posts one of DotDetectedEvent, DashDetectedEvent, BadPulseEvent

 Notes

 Author
     Bibit Bianchini, 11/01/2018
****************************************************************************/
static void CharacterizePulse(void) {
	// Calculate LastPulseWidth as TimeOfLastFall - TimeOfLastRise
	uint16_t LastPulseWidth = (TimeOfLastFall - TimeOfLastRise);
	// If LastPulseWidth OK for a Dot
	if ((LastPulseWidth>=(LengthOfDot-1)) &
      (LastPulseWidth<=(LengthOfDot+1))) {
		// PostEvent DotDetected Event to Decode Morse Service
		ES_Event_t ThisEvent;
		ThisEvent.EventType = DotDetected;
		PostDecodeMorse(ThisEvent);
	}
	else {
		// If LastPulseWidth OK for dash
		if ((LastPulseWidth>=((3*LengthOfDot)-3)) &
        (LastPulseWidth<=((3*LengthOfDot)+3))) {
			// PostEvent DashDetected Event to Decode Morse Service
			ES_Event_t ThisEvent;
			ThisEvent.EventType = DashDetected;
			PostDecodeMorse(ThisEvent);
		}
		else {
			// PostEvent BadPulse Event to Decode Morse Service
			ES_Event_t ThisEvent;
			ThisEvent.EventType = BadPulse;
			PostDecodeMorse(ThisEvent);
		}
	}
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
