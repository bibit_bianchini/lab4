/***************************************
Test harness for LCD Initialization
Lab 3, Part 3.6
***************************************/


#define TEST2


#ifdef TEST2
#include "termio.h"
#include "ES_Port.h"
#include "LCD_Write.h"

int main(void)
{
  TERMIO_Init();
  puts("\r\n\n\nIn test harness for step-by-step LCD Initialization Sequence\r\n");
  
 	printf("%s %s\n",__TIME__, __DATE__);
 	printf("\n\r\n");
  
  LCD_HWInit();
	
	uint16_t Delay = 1;
	
	while(Delay) {
		Delay = LCD_TakeInitStep();
		getchar();
	}
	
	printf("Finished LCD Initialization Sequence\r\n");
  
	printf("Going to print \"HELLO WORLD\"...\r\n");
	
	getchar();
	LCD_WriteData8(0x48);
	printf("H");
	
	getchar();
	LCD_WriteData8(0x45);
	printf("E");
	
	getchar();
	LCD_WriteData8(0x4c);
	printf("L");
	
	getchar();
	LCD_WriteData8(0x4c);
	printf("L");
	
	getchar();
	LCD_WriteData8(0x4f);
	printf("O");
	
	getchar();
	LCD_WriteData8(0x20);
	printf(" ");
	
	getchar();
	LCD_WriteData8(0x57);
	printf("W");
	
	getchar();
	LCD_WriteData8(0x4f);
	printf("O");
	
	getchar();
	LCD_WriteData8(0x52);
	printf("R");
	
	getchar();
	LCD_WriteData8(0x4c);
	printf("L");
	
	getchar();
	LCD_WriteData8(0x44);
	printf("D");
	
}

#endif    // TEST2
